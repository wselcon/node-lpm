#!/usr/bin/env node

const net = require('net')
const program = require('commander')
const pkg = require('../package.json')
const LPM = require('../')
const debug = require('debug')('lpm:sendlpm')

program
  .usage('[options] <message> [otherMessages ...]')
  .version(pkg.version)
  .option('-p, --port <port>', 'TCP Port. Defaults to 5511', parseInt)
  .option('-h, --host <host>', 'IP or Address to the host communicating with. Defaults to 127.0.0.1')
  .option('-t --timeout <millisecond>', 'timeout in milliseconds to wait for reply')
  .option('-d --delay <milliseconds>', 'delay between each message. if > timeout that this value will be used for timeout. Defaults to 0')
  .parse(process.argv)

if (!process.argv.slice(2).length) {
  console.log(program.outputHelp())
  process.exit(1)
}

const HOST = program.host || '127.0.0.1'
const PORT = program.port || 5511
const DELAY = program.delay || 0
let TIMEOUT = program.timeout || 10000

let clients = []
let sent = 0
let received = 0

if (DELAY > TIMEOUT) {
  TIMEOUT = DELAY
}

function checkAllDone () {
  debug(`checkAllDone   ${sent} === ${received}, ${clients.length}`)
  if (sent === received && clients.length > 0) {
    console.log('\nSuccess!')
    clients.forEach((client) => {
      client.destroy()
    })
    clients = []
    return true
  } else {
    return false
  }
}

function showMessage (text) {
  console.log(`Received reply: ${text}`)
}

function connectAndSend (output, wait) {
  const client = new net.Socket()
  clients.push(client)
  client.setTimeout(TIMEOUT)
  sent += output.length
  debug('%s on queue', sent)
  client.on('data', (data) => {
    const lpm = new LPM(data)
    if (debug.enabled) {
      debug('on data', data, debug.enabled)
    }
    if (lpm.EOF) {
      debug('nothing to receive', data)
    }
    let count = 0
    while (!lpm.EOF) {
      const text = lpm.readString()
      debug(`Got message in reply: '${text}'`)
      count++
      received++
      showMessage(text)
    }
    debug(`${count} messages in reply`)
    checkAllDone()
  })

  client.on('error', (err) => {
    console.log('Unexpected socket error', err)
    client.destroy()
  })

  client.on('timeout', () => {
    if (!checkAllDone()) {
      console.log('Timeout waiting for reply')
    }
    client.destroy()
  })

  setTimeout(() => {
    debug('connecting', HOST, PORT)
    client.connect(PORT, HOST, () => {
      debug('connected')
      const lpm = new LPM()
      output.forEach((arg) => {
        lpm.write(arg)
      })

      console.log(`\nSending ${output.length} message(s): ${output.join(', ')}`)
      client.write(lpm.buffer)
    })
  }, wait || 0)
}

if (DELAY > 0 && program.args.length > 1) {
  console.log(`Sending ${program.args.length} separated by ${DELAY} ms`)
  program.args.forEach((arg, index) => {
    connectAndSend([arg], DELAY * index)
  })
} else {
  connectAndSend(program.args)
}
