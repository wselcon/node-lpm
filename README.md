LPM
===
Library + CLI-tcp-client for Length prefixed messages


# Usage
```
Usage: sendlpm [options] <message> [otherMessages ...]

Options:
  -V, --version               output the version number
  -p, --port <port>           TCP Port. Defaults to 5511
  -h, --host <host>           IP or Address to the host communicating with. Defaults to 127.0.0.1
  -t --timeout <millisecond>  timeout in milliseconds to wait for reply
  -d --delay <milliseconds>   delay between each message. if > timeout that this value will be used for timeout. Defaults to 0
  -h, --help                  output usage information
```


# Examples
```powershell
node .\bin\sendlpm.js -d 3000 StartNewScan StartMapping LatestAprilTag "StopMappingAndProcess c:\\scan-data\slask.ply 0 1"
```
