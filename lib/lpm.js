/**
 * Length-Prefix-Message
 *
 */
const debug = require('debug')('dot3d:lpm')

class LPM {
  constructor (buffer) {
    this.readable = false
    this.offset = 0
    this.buffer = null
    this.debug = debug
    if (Buffer.isBuffer(buffer)) {
      this.readable = true
      this.buffer = buffer
    }
    if (debug.enabled) {
      debug(`new Lpm(${buffer.length} bytes')`)
    }
  }

  get length () {
    if (this.buffer) {
      return this.buffer.length
    }
    // if we have no buffer this can't be anything but.
    return 0
  }

  get EOF () {
    return this.offset >= this.length
  }

  _concat (buffer) {
    this.offset += buffer.length
    if (this.buffer && this.buffer.length > 0) {
      this.buffer = Buffer.concat([this.buffer, buffer])
    } else {
      this.buffer = buffer
    }
    return this
  }

  _readUInt32 () {
    const value = this.buffer.readUInt32BE(this.offset)
    this.offset += 4
    return value
  }

  readString (encoding) {
    let result = ''
    const enc = encoding || 'utf8'

    const length = this._readUInt32()
    result = this.buffer.toString(enc, this.offset, this.offset + length)
    if (debug.enabled) {
      debug(`reading sized, length: ${length}, start: ${this.offset}, end:${this.offset + length} result: ${result.replace('\n', '\\n')}`)
    }
    this.offset += length
    return result
  }

  writeString (text) {
    if (typeof text !== 'string') {
      throw new TypeError('#writeString only accepts strings')
    }
    let data = Buffer.from(text)
    if (this.debug.enabled) { this.debug(`writeString('${text.replace('\n', '\\n')}':${text.length}) as ${this.stringtype}`) }

    const size = Buffer.alloc(4)
    size.writeUInt32BE(text.length)
    data = Buffer.concat([size, data])

    return this._concat(data)
  }

  write (data) {
    if (typeof data === 'string') {
      return this.writeString(data)
    }
    throw new Error(`Writing '${typeof data}' type is not implemented`)
  }

  static write (data) {
    const lpm = new LPM()
    return lpm.write(data)
  }
}

module.exports = LPM
